module gitee.com/go-wena/json

go 1.16

require (
	github.com/goccy/go-json v0.7.2
	github.com/json-iterator/go v1.1.11
)
