# GO JSON Package

```
go build -tag <tags>

tags:
    jsoniter: github.com/json-iterator/go
    go_json:  github.com/goccy/go-json
    default:  encoding/json
```