//go:build go_json
//+build go_json

package json

import (
	"github.com/goccy/go-json"
)

var (
	Marshal       = json.Marshal
	MarshalIndent = json.MarshalIndent
	Unmarshal     = json.Unmarshal
	NewDecoder    = json.NewDecoder
	NewEncoder    = json.NewEncoder
	HTMLEscape    = json.HTMLEscape
)

type (
	RawMessage = json.RawMessage
	Decoder    = json.Decoder
	Encoder    = json.Encoder
)
