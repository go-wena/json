//go:build jsoniter
//+build jsoniter

package json

import (
	"encoding/json"

	"github.com/json-iterator/go"
)

var (
	Marshal       = jsoniter.Marshal
	MarshalIndent = jsoniter.MarshalIndent
	Unmarshal     = jsoniter.Unmarshal
	NewDecoder    = jsoniter.NewDecoder
	NewEncoder    = jsoniter.NewEncoder
	HTMLEscape    = json.HTMLEscape
)


type (
	RawMessage = jsoniter.RawMessage
	Decoder    = jsoniter.Decoder
	Encoder    = jsoniter.Encoder
)
