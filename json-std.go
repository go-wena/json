//go:build !jsoniter && !go_json
// +build !jsoniter,!go_json

package json

import (
	"encoding/json"
)

var (
	Marshal       = json.Marshal
	MarshalIndent = json.MarshalIndent
	Unmarshal     = json.Unmarshal
	NewDecoder    = json.NewDecoder
	NewEncoder    = json.NewEncoder
	HTMLEscape    = json.HTMLEscape
)

type (
	RawMessage = json.RawMessage
	Decoder    = json.Decoder
	Encoder    = json.Encoder
)
